#!/bin/sh

# Iterate through all tracked markdown files in the source directory.
# $1 Path to source directory
# $2 Path to output directory
# $3 Path to templates directory
generate_articles() {
    mkdir -p "$2/articles"
    for md in $(git ls-tree -r HEAD --name-only "$1" | grep -G ".*\.md")
    do
        page_name=$(basename "$md" ".md")
        generate_article_page "$md" "$2" "$3" "$page_name"
        generate_index_entry "$md" "$page_name"
    done
}

# Generate an article list entry for the given markdown file.
# $1 Relative path to markdown file from repo root
# $2 File name base for output
generate_index_entry() {
    date_published=$(git log --diff-filter=A --format=%as "$1" | tail -1)
    timestamp_published=$(git log --diff-filter=A --format=%at "$1" | tail -1)
    date_updated=$(git log --diff-filter=M --format=%as "$1" | head -1)
    title=$(grep -G "^# .*$" "$1" | cut -c 3-)
    outname="$timestamp_published-entry-$2"
    echo "<li><a href="articles/$2.html">$title</a><br><p class="date">Published $date_published." |
        tee -a "tmp/$outname.html" 1> /dev/null
    if [ -n "$date_updated" ]
    then
        echo " Updated $date_updated." | tee -a "tmp/$outname.html" 1> /dev/null
    fi
    echo "</p></li>" | tee -a "tmp/$outname.html" 1> /dev/null
}

# Generate an article page for the given markdown file. The latest committed
# version of the file will be used. Uncommitted changes to the markdown file
# will not be present in the HTML output.
# $1 Relative path to markdown file to convert from repo root
# $2 Path to output directory
# $3 Path to templates directory
# $4 File name base for output
generate_article_page() {
    head=$(cat "$3/head.html")
    end=$(cat "$3/end.html")
    page_title=$(grep -G "^# .*$" "$1" | cut -c 3-)
    file_contents=$(git show HEAD:"$1")
    echo "$(echo "$head" | sed -e "s/<title>/<title>$page_title - /")
        $(echo "$file_contents" | lowdown -T html -) $end" |
        tidy -ibq --tidy-mark false --indent-spaces 4 > "$2/articles/$4.html"
}

# Generate a blog index page.
# $1 Path to output directory
# $2 Path to templates directory
generate_blog_index() {
    head=$(cat "$2/head.html")
    end=$(cat "$2/end.html")
    blog_header=$(cat "$2/blog_header.html")
    blog_footer=$(cat "$2/blog_footer.html")
    list=$(cat $(find tmp -iregex ".*\.html" | sort -r))
    echo "$head $blog_header $list $blog_footer $end" |
        tidy -ibq --tidy-mark false --indent-spaces 4 > "$1/blog.html"
}

# Generate an RSS feed.
# $1 Path to source directory
# $2 Path to output directory
# $3 Path to templates directory
# $4 Root URL for site
generate_rss() {
    items=""
    for article in $(git ls-tree -r HEAD --name-only "$1" | grep -G ".*\.md")
    do
        page_name=$(basename "$article" ".md")
        date=$(git log --diff-filter=A --format=%aD "$article" | tail -1)
        title=$(grep -G "^# .*$" "$article" | cut -c 3-)
        link="$4/articles/$page_name.html"
        description=$(git show HEAD:"$article" | lowdown -T html -)
        items="$items <item><title>$title</title><pubDate>$date</pubDate><link>$link</link><description><![CDATA[$description]]></description></item>"
    done

    rss_head=$(cat "$3/rss_head.xml")
    rss_end=$(cat "$3/rss_end.xml")
    echo "$rss_head $items $rss_end" |
        tidy -ibq -xml --tidy-mark false --indent-spaces 4 > "$2/rss.xml"
}

usage() {
    echo "Usage: sbg src dst https://example.tld"
    exit
}

# $1 Source directory
# $2 Output directory
# $3 Root URL for the site
main() {
    # Ensure all required arguments are present or print usage
    test -n "$1" || usage
    test -n "$2" || usage
    test -n "$3" || usage

    # Create output directory if it does not already exist. Source directory
    # must already exist to have anything to generate.
    test -d "$2" || mkdir "$2"

    src=$(readlink -f "$1")
    dst=$(readlink -f "$2")
    templates=$(readlink -f "templates")

    mkdir -p tmp
    generate_articles "$src" "$dst" "$templates"
    generate_blog_index "$dst" "$templates"
    rm -r tmp
    generate_rss "$src" "$dst" "$templates" "$3"
}

main "$@"

